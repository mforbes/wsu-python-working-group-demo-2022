# CoCalc Account

[CoCalc] is an online computing platform that allows collaborative computations.  I
highly recommend you create an account and explore it.  For more details, see
{ref}`sec:cocalc`, but please keep in mind that these notes are from a previous course
and may not be completely relevant or up-to-date.

:::{admonition} About CoCalc
:class: dropdown

[CoCalc] is an online platform that allows you to run code in the cloud on fully
provisioned virtual machines.  These machines run Linux, and a [huge array of
software](https://cocalc.com/features), including the full [Anaconda] python stack.

The unique advantage of [CoCalc] is collaboration: it uses a custom interface to
[Jupyter] notebooks that allows users to collaborate in real-time, executing the same
code, and sharing the same output.  To my knowledge, no other system allows such
interactivity.

Similar collaborative interfaces exist for
[LaTeX](https://cocalc.com/features/latex-editor),
[R](https://cocalc.com/features/r-statistical-software),
[Octave](https://cocalc.com/features/octave),
[Julia](https://cocalc.com/features/julia),
[Sage](https://cocalc.com/features/sage), and others.

Brand new is a collaborative [Whiteboard](https://cocalc.com/features/whiteboard)
which allows you to run code, and then annotate the output.  This was a feature
[CoCalc] developed for us to help teach, and will soon have a PDF annotation feature
so that we can use it for collaborative analysis and commenting on lecture notes,
research papers, etc.

[CoCalc] is also a complete computing platform: you can install whatever software you
need, and you can [use SSH](https://doc.cocalc.com/account/ssh.html) to connect to your
project with a terminal, or edit files remotely with
[Tramp](https://www.emacswiki.org/emacs/TrampMode) or similar tools.  It also has the
most amazing backup system via a Time Travel feature that allows you to roll back
your changes by the minute, hour, day, week, month etc.  This alone makes the
platform worthwhile and has saved me several times from data loss.

Creating an account on [CoCalc] is free, but the free projects do not have internet
access, and will run slowly.  Their business model is to charge for computing
resources, and this is quite modest -- ~$40/year for a simple license that you can
use to run one project at a time.  (You are free to use this on as many projects as
you have, but only 1 can run simultaneously under a simple license -- you may have to
shut others down.)

The software behind [CoCalc] is also open-source, and you can run a version on your
own hardware [with Docker](https://doc.cocalc.com/docker-image.html) if you need to,
so there is no "lock-in".  The team is also small enough that they respond to
requests -- often within hours.  This is extremely refreshing compared with large
companies where you submit requests to a forum to have them ignored for years...

There are a couple of downsides to using [CoCalc].  Running everything remotely means
that an unstable internet connection can make things difficult.  Also, unless you
purchase sufficient computing resources, your laptop/desktop is likely going to
perform better.  For this reason, I usually develop alone on my computer, then use
my VCS to push everything to the cloud, and ultimately to [CoCalc] where I share my
results with collaborators/students (and take advantage of the backup capabilities).

Another "problem" is due to [CoCalc]'s huge feature set.  A colleague likened it to
getting behind the controls of a fighter jet: there so many options that it can be
confusing.  For this reason, other tools like [Overleaf](https://www.overleaf.com/)
might be preferable for collaborating on LaTeX documents with less tech-savvy
colleagues.  But [CoCalc] is so much more powerful, that it is worth considering.
(The advantages immediately become clear when working on a complex multi-part
document.)

Despite these downsides, I highly recommend [CoCalc].  It provides an extremely fast
way to explore ideas, and ultimately has become an integral part of our workflow.
The collaborative opportunities are unprecedented, and there are none of the
downsides of other collaborative tools which often lock you in to using their
products.
:::

## Get a CoCalc Account

Create an account on [CoCalc] and sign in.  For the seminar, we will share a provisioned
product that you can copy for your own use later.  Note: the free account will allow you
to work with your project, running code (slowly), etc. but will lack internet access
(people were abusing this).  You can have full access for a fairly modest cost
(~$40/year).

## Create CoCalc Project

* Add License.
* Add SSH keys (so that we can pull from [GitLab].
* Copy key `c892e42f-a2db-4bf8-a298-caeb58d246e8` to `~/.ssh/config`:

  ```bash
  # ~/.ssh/config 
  Host ccpwg
    User c892e42fa2db4bf8a298caeb58d246e8
    
  Host cc*
    HostName ssh.cocalc.com
    ForwardAgent yes
    SetEnv LC_HG_USERNAME="Michael McNeil Forbes <m.forbes+pwg@wsu.edu>
    SetEnv LC_GIT_USERNAME="Michael McNeil Forbes"
    SetEnv LC_GIT_USEREMAIL="m.forbes+pwg@wsu.edu"
    SetEnv LC_EDITOR=vi
    
  Host *
    IgnoreUnknown UseKeychain
    UseKeychain yes
    AddKeysToAgent yes
    AddressFamily inet
    # Force IPv4
    # https://www.electricmonk.nl/log/2014/09/24/
    #         ssh-port-forwarding-bind-cannot-assign-requested-address/
  ```

[Miniconda]: <https://docs.conda.io/en/latest/miniconda.html>
[Anaconda]: <https://www.anaconda.com/>
[Mercurial]: <https://www.mercurial-scm.org/>
[Git]: <https://git-scm.com/>
[CoCalc]: <https://cocalc.com/>
[Mamba]: <https://mamba.readthedocs.io/>
[Windows Subsystem for Linux]: <https://docs.microsoft.com/en-us/windows/wsl/about>
[GitLab]: <https://gitlab.com>
[GitHub]: <https://github.com>
[Heptapod]: <https://hg.iscimath.org>
[Read the Docs]: <https://readthedocs.org>
[Conda]: <https://docs.conda.io/en/latest/>
