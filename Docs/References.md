Readings
========

## Textbook

The course will make regular reference to {cite:p}`Gezerlis:2020`.

(sec:references)=
References
==========

```{bibliography}
:style: alpha
```
