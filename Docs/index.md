<!-- WSU PWG Demo 2022
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.
-->

```{include} PWG_Seminar_18Apr2022.md
``` 

```{toctree}
---
maxdepth: 2
caption: "Starting From Scratch:"
---
0_CoCalc
1_Repos
2_Cookiecutter
```

```{toctree}
---
maxdepth: 3
caption: "Getting to Work:"
---
3_CoCalc
```


```{toctree}
---
maxdepth: 2
caption: "Miscellaneous:"
hidden:
---
References
Demonstration
CoCalc
README.md <../README>
```

