# Cookiecutter Project Templates

Now we download one of the cookiecutter configuration files from our repository:

* https://hg.iscimath.org/forbes-group/cookiecutters/project

and modify it to represent our current project.  Then use this to generate the project.
For details, see the [README.md](https://hg.iscimath.org/forbes-group/cookiecutters/).

```bash
wget -O .cookiecutter.yaml https://hg.iscimath.org/forbes-group/cookiecutters/-/raw/topic/default/initial/project/code-repo.yaml?inline=false
hg add .cookiecutter.yaml
hg com -m "Initial version of .cookiecutter.yaml"
vi .cookiecutter.yaml
hg com -m "Updated .cookiecutter.yaml: ready for skeleton"
cookiecutter hg+https://hg.iscimath.org/forbes-group/cookiecutters \
             --directory project                                   \
             --config-file .cookiecutter.yaml                      \
             --overwrite-if-exists                                 \
             --no-input
```

Since we left the `README.md` file, this should complain with an error `UserWarning: Not
adding symlink: dir has non-dot files ['README.md']`.  We do this as a test -- the
generated project will end up in a folder -- `wsu-pwg-demo-2022` in this case:

```bash
cd wsu-pwg-demo-2022
make init  # Should build the environment, install the kernel, etc.
make html  # Generates the documentation.
```

If everything looks good, then remove this folder and the `README.md`, and do this again:

```bash
rm -rf README.md wsu-pwg-demo-2022
cookiecutter hg+https://hg.iscimath.org/forbes-group/cookiecutters \
             --directory project                                   \
             --config-file .cookiecutter.yaml                      \
             --overwrite-if-exists                                 \
             --no-input
```

Now everything should be unpacked in the current project directory without a warning.
Create a new branch for these updates and add and commit the changes, then merge these
back into the default branch.

```bash
hg branch "cookiecutter-base"
hg add
hg com -m "BASE: Initial cookiecutter template"
hg up default
hg merge cookiecutter-base
hg com -m "Merge in cookiecutter updates"
```

The reason for this new branch `cookiecutter-base` is that it will contain any future
updates to the templates.  Because we merged this back into our default branch, the VCS
should know about any changes, and be able to merge updates in with minimal conflict.

Now push everything back to [GitLab]:

```bash
hg bookmark main  # For hg-git
hg push
```

If all goes well, this should trigger [Read The Docs] to build the documentation:

* https://readthedocs.org/projects/wsu-python-working-group-demo-2022/builds/

This provides an independent test that the project environment is complete, etc.  After
a few minutes, you should be able to see the new docs: 

* https://wsu-python-working-group-demo-2022.readthedocs.io/en/latest/


[Miniconda]: <https://docs.conda.io/en/latest/miniconda.html>
[Anaconda]: <https://www.anaconda.com/>
[Mercurial]: <https://www.mercurial-scm.org/>
[Git]: <https://git-scm.com/>
[CoCalc]: <https://cocalc.com/>
[Mamba]: <https://mamba.readthedocs.io/>
[Windows Subsystem for Linux]: <https://docs.microsoft.com/en-us/windows/wsl/about>
[GitLab]: <https://gitlab.com>
[GitHub]: <https://github.com>
[Heptapod]: <https://hg.iscimath.org>
[Read the Docs]: <https://readthedocs.org>
[Conda]: <https://docs.conda.io/en/latest/>
