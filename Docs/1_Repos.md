## Repositories

For a serious project, the first step we do is to create a repository online at either
our internal [Heptapod] server, [GitLab], or [GitHub].  This is optional, but highly
recommended for the purposes of backing up your data, running automated tests, building
and hosting documentation, etc.  Here we will do it on [GitLab], which will result in a
repository like this:

* [https://gitlab.com/mforbes/wsu-python-working-group-demo-2022](https://gitlab.com/mforbes/wsu-python-working-group-demo-2022)

:::{toggle}
In the seminar, we will create a project with the following info (here for cut and paste):

* <https://gitlab.com/projects/new#blank_project>

```
WSU Python Working Group Demo 2022

Demonstration of our scientific working procedure for the WSU Python Working Group:
Monday 18 April 2022
Public
Keep README
```
:::

:::{margin}
Note: this is a [Git] repo, but I prefer using Mercurial, so I use the
[hg-git] bridge installed above.
:::
Now we clone this to our local machine.  I have SSH keys setup, so I can use that
interface.

```bash
hg clone git@gitlab.com:mforbes/wsu-python-working-group-demo-2022.git
cd wsu-python-working-group-demo-2022
```

::::{admonition} Optional Steps
You might want to push-mirror this repo to [GitHub] or another site.  This can be done
later.

If you want to host your documentation on [Read the Docs], then you might want to sync
this up now so you can get the slug and URL.

:::{toggle}
In the seminar, we go to:

* https://readthedocs.org/dashboard/import
* Refresh list, filter by red mforbes, and choose demo.
* Mention short title.
* View docs, and copy URL: https://wsu-python-working-group-demo-2022.readthedocs.io/en/latest/
:::
::::

[Miniconda]: <https://docs.conda.io/en/latest/miniconda.html>
[Anaconda]: <https://www.anaconda.com/>
[Mercurial]: <https://www.mercurial-scm.org/>
[Git]: <https://git-scm.com/>
[CoCalc]: <https://cocalc.com/>
[Mamba]: <https://mamba.readthedocs.io/>
[Windows Subsystem for Linux]: <https://docs.microsoft.com/en-us/windows/wsl/about>
[GitLab]: <https://gitlab.com>
[GitHub]: <https://github.com>
[Heptapod]: <https://hg.iscimath.org>
[Read the Docs]: <https://readthedocs.org>
[Conda]: <https://docs.conda.io/en/latest/>
[hg-git]: <https://hg-git.github.io/>
